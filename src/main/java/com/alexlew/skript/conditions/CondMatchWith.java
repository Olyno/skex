package com.alexlew.skript.conditions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Condition;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import org.bukkit.event.Event;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Name("Match")
@Description("Checks if a string matches with a regex/pattern")
@Examples({
		""
})
@Since("1.0")

public class CondMatchWith extends Condition {

	static {
		Skript.registerCondition(CondMatchWith.class,
				"%strings% match[es] with %string%",
				"%strings% does(n't| not) match with %string%"
		);
	}

	private Expression<String> strings;
	private Expression<String> regex;

	@Override
	@SuppressWarnings("unchecked")
	public boolean init(Expression<?>[] expr, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
		strings = (Expression<String>) expr[0];
		regex = (Expression<String>) expr[1];
		setNegated(matchedPattern == 1);
		return true;
	}

	@Override
	public boolean check(Event e) {
		Pattern pattern = Pattern.compile(regex.getSingle(e));
		Object[] exprs = strings.getArray(e);
		for (Object expr : exprs) {
			Matcher matcher = pattern.matcher((String) expr);
			if (!matcher.find() && !isNegated()) return false;
			else if (matcher.find() && isNegated()) return false;
		}
		return true;
	}

	@Override
	public String toString(Event e, boolean debug) {
		return getClass().getName();
	}
}
