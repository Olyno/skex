package com.alexlew.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import org.bukkit.event.Event;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Name("Matched groups")
@Description("Returns matched groups as string.")
@Examples({
		""
})
@Since("1.0")

public class ExprMatchedGroups extends SimpleExpression<String> {

	static {
		Skript.registerExpression(ExprMatchedGroups.class, String.class, ExpressionType.SIMPLE,
				"matched group[s] of %string% with [(regex|pattern)] %string%"
		);
	}

	private Expression<String> string;
	private Expression<String> regex;

	@Override
	@SuppressWarnings("unchecked")
	public boolean init(Expression<?>[] expr, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
		string = (Expression<String>) expr[0];
		regex = (Expression<String>) expr[1];
		return true;
	}

	@Override
	protected String[] get(Event e) {
		Pattern pattern = Pattern.compile(regex.getSingle(e), Pattern.MULTILINE);
		Matcher matcher = pattern.matcher(string.getSingle(e));
		if (matcher.find()) {
			List<String> groups = new LinkedList<>();
			for (int i = 0; i <= matcher.groupCount(); i++) {
				groups.add(matcher.group(i));
			}
			return groups.toArray(new String[groups.size()]);
		} else {
			return null;
		}
	}

	@Override
	public boolean isSingle() {
		return false;
	}

	@Override
	public Class<? extends String> getReturnType() {
		return String.class;
	}

	@Override
	public String toString(Event e, boolean debug) {
		return getClass().getName();
	}
}
